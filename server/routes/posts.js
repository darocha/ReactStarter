import { connectionString } from '../connection-string';
import express from 'express';
import monk from 'monk';

var router = express.Router();
var db = monk(connectionString);

router.get('/api/posts', (req, res) => {

    var collection = db.get('Posts');

    // collection.insert({ title: 'azure' });

    collection.find({}, (err, posts) => {
        if (err) throw err;

        console.log(posts);
      	res.json(posts);
    });

});

module.exports = router;
