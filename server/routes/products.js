import { connectionString } from '../connection-string';
import express from 'express';
import monk from 'monk';
import restful from "node-restful";

var app = express();
 
var mongoose = restful.mongoose;
    // mongoose.Promise = global.Promise;
    mongoose.connect(connectionString)
    
    //.then(() =>  console.log('connection succesful'))
    //.catch((err) => console.error(err));
    

var ProductSchema = mongoose.Schema({
    name: String,
    sku: String,
    price: Number
});

var Product = restful.model('Products', ProductSchema);
    Product.methods(['get','put','post','delete']);
    Product.register(app,'/api/products');


function createProduct()
{
    // Create a product in memory
    var product = new Product({name: 'Azure', sku: 1, price: 100.00 });

    // Save it to database
    product.save(function(err) {
        
        if(err)
            console.log(err);
        else
            console.log(product);

    });
}

module.exports = app;

/*
    Registers the following routes:
    GET /products
    GET /products/:id
    POST /products
    PUT /products/:id
    DELETE /products/:id
*/
