import { connectionString } from '../connection-string';
import express from 'express';
import monk from 'monk';

export var router = express.Router();
var db = monk(connectionString);

router.get('/api/users', (req, res) => {
    var collection = db.get('Users');
    collection.find({}, (err, posts) => {
        if (err) throw err;
      	res.json(posts);
    });
});


