// import { connectionString } from './connection-string';
import express from 'express';
import path from 'path';
import favicon from 'serve-favicon';
import logger from 'morgan';
import cookieParser from 'cookie-parser'; 
import bodyParser from 'body-parser';
import mongo from 'mongodb';
//import monk from 'monk';

//var db = monk(connectionString);

import home from './routes/home';
import posts from './routes/posts';
import products from './routes/products';

export const app = express();

// read values from web.config applicationSettings
// var virtualDirPath = process.env.virtualDirPath || '';

// uncomment after placing your favicon in /public
// app.use(favicon(__dirname + '/public/favicon.ico'));
// app.use(logger('dev'));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

// Make our db accessible to our router

/*
app.use((req, res, next) => {
    req.db = db;
    next();
});
*/

app.use(home);
app.use(posts);
app.use(products);

/// catch 404 and forwarding to error handler
app.use((req, res, next) => {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers will print stacktrace
if (app.get('env') === 'development') {
    app.use((err, req, res, next) => {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler no stacktraces leaked to user
app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

