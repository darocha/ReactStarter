var MongoClient = require('mongodb').MongoClient
  , assert = require('assert');

// Connection URL
var url = 'mongodb://localhost:27017/Blog';

url = "mongodb://darochadb:2FOwowQfIOb9usXJgMIY6faOnPHp1io681J2VMhJ1Pxd4lXg7i5ITsotxy0Gm8zbzsDuqt5rnSI8YtERTi99wA==@darochadb.documents.azure.com:10250/?ssl=true";

// Use connect method to connect to the server
MongoClient.connect(url, function(err, db) {
  
  assert.equal(null, err);
  console.log("Connected successfully to server");

  findDocuments(db, null);

  db.close();

});


function insertDocuments(db, callback) {
  // Get the documents collection
  var collection = db.collection('Posts');
  // Insert some documents
  collection.insertMany([    
      {a : 1}, {a : 2}, {a : 3}
  ], function(err, result) {
    assert.equal(err, null);
    assert.equal(3, result.result.n);
    assert.equal(3, result.ops.length);
    console.log("Inserted 3 documents into the collection");
    //callback(result);
  });
}

function findDocuments(db, callback) {
  // Get the documents collection
  var collection = db.collection('Posts');
  // Find some documents
  collection.find({}).toArray(function(err, docs) {
    assert.equal(err, null);
    console.log("Found the following records");
    console.log(docs)
    //callback(docs);
  });

}