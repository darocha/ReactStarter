<?xml version="1.0" encoding="utf-8"?>  
<configuration>  
      <appSettings file="appSettings.config">
            <add key="BABEL_DISABLE_CACHE" value="true" />
      </appSettings>
      <system.web>
           <customErrors mode="Off"/>
      </system.web>
      <system.webServer>           
      <handlers>  
           <add name="node_express" path="server/index.js" verb="*" modules="iisnode"/>  
     </handlers>  
      <rewrite>  
           <rules>  
                <!-- force https -->
                <rule name="Redirect to https" stopProcessing="true">
                    <match url="(.*)" />
                    <conditions>
                        <add input="{HTTPS}" pattern="^off$" ignoreCase="true" />
                    </conditions>
                    <action type="Redirect" url="https://{HTTP_HOST}{REQUEST_URI}" redirectType="Permanent" appendQueryString="false" />
                </rule>
                <!-- cannonical remove www -->
                <rule name="Remove www" stopProcessing="true">
                    <match url="(.*)" ignoreCase="true" />
                    <conditions logicalGrouping="MatchAll">
                        <add input="{HTTP_HOST}" pattern="^www\.(.+)$" />
                    </conditions>
                    <action type="Redirect" url="https://{C:1}/{R:0}" appendQueryString="true" redirectType="Permanent" />
                </rule>
                <!-- logging -->
                <rule name="LogFile" patternSyntax="ECMAScript" stopProcessing="true">  
                     <match url="iisnode"/>  
                </rule>  
                <!-- debugging  -->
                <rule name="NodeInspector" patternSyntax="ECMAScript" stopProcessing="true">                      
                    <match url="server/index.js\/debug[\/]?" />  
                </rule>  
                <!-- node express server/index.js -->
                <rule name="ExpressAPI" stopProcessing="true">
                    <match url="^(api)" />
                    <action type="Rewrite" url="server/index.js" />
                </rule>
                <!-- socketio server/index.js -->
                <rule name="SocketIO" patternSyntax="ECMAScript" stopProcessing="true">
                    <match url="socket.io.+" />
                    <action type="Rewrite" url="server/index.js"/>
                </rule>
                <!-- serve static files from /public folder: ex: /index.html will serve /public/index.html  -->
                <rule name="StaticContent" >
                    <conditions logicalGrouping="MatchAll">
                        <add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" />
                    </conditions>
                    <action type="Rewrite" url="build{REQUEST_URI}"  />
                </rule>
                <!-- single page app redirect all requests to public/index.html -->
                <rule name="Angular" stopProcessing="true">
                  <match url=".*" />
                  <conditions logicalGrouping="MatchAny" >
                        <add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
                        <add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="false" />
                  </conditions>
                  <action type="Rewrite" url="build/index.html" />
                </rule>
           </rules>  
      </rewrite>
      
      <!-- exclude node_modules directory and subdirectories from serving  by IIS since these are implementation details of node.js applications -->
      <security>
       <requestFiltering>
         <hiddenSegments>
           <add segment="node_modules" />
         </hiddenSegments>
       </requestFiltering>
     </security> 

    <httpCompression directory="%SystemDrive%\inetpub\temp\IIS Temporary Compressed Files" cacheControlHeader="true" sendCacheHeaders="true">
      <scheme name="gzip" dll="%Windir%\system32\inetsrv\gzip.dll" staticCompressionLevel="9" />
      <dynamicTypes>
        <add mimeType="text/*" enabled="true" />
        <add mimeType="message/*" enabled="true" />
        <add mimeType="application/x-javascript" enabled="true" />
        <add mimeType="application/json" enabled="true" />
        <add mimeType="application/json; charset=utf-8" enabled="true" />  
        <add mimeType="*/*" enabled="false" />
      </dynamicTypes>
      <staticTypes>
        <add mimeType="text/*" enabled="true" />
        <add mimeType="message/*" enabled="true" />
        <add mimeType="application/json" enabled="true" />
        <add mimeType="application/json; charset=utf-8" enabled="true" />  
        <add mimeType="application/x-javascript" enabled="true" />
        <add mimeType="application/atom+xml" enabled="true" />
        <add mimeType="application/xaml+xml" enabled="true" />
        <add mimeType="*/*" enabled="false" />
      </staticTypes>
    </httpCompression>
    <urlCompression doStaticCompression="true" doDynamicCompression="true" />

   </system.webServer>  
 </configuration>